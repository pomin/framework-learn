//
//  AppDelegate.h
//  BeeFramework
//
//  Created by heyun on 13-7-31.
//  Copyright (c) 2013年 heyun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
